var kelvin = 273.15;
var city = '';
var lat = 0;
var lon = 0;
var ready = 0;
var cityid = 0;
var date = new Date();
var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
var keys = ["2197fa983811daaf36ee85f5abe2e6e5" , "3c6ad9980b87cb48863ad4b39e6cb409", "c4a876ab2eacac2336ab19aa4f9c8d17", "3a2b1bac6cd2214eedad37e6dc77ad79" , "d7eb6fc34f78bb0907635c72756c78a3"];

//shuffle api keys
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

//openweather api call by city
const asyncWithJquery = () => {
    $.get('https://api.openweathermap.org/data/2.5/weather?APPID='+ key[2] +'&q='+city)
        .done(asycnSuccessCallbackHandler)
        .fail(asycnFailCallbackHandler);
}

//openweather api call by coords
const getWeatherByCoords = (lat,lon) => {
    $.get('https://api.openweathermap.org/data/2.5/weather?APPID=3c6ad9980b87cb48863ad4b39e6cb409&lat='+lat+'&lon='+lon)
        .done(asycnSuccessCallbackHandler)
        .fail(asycnFailCallbackHandler);
}

//restcountries api call by alphacode
const getCountryInfo = (alphacode) => {
    $.get('https://restcountries.eu/rest/v2/alpha/'+alphacode)
        .done(succesCountryInfo)
        .fail(asycnFailCallbackHandler);
}

//jquery call is successfull
const asycnSuccessCallbackHandler = (data) => {
    //console.log(data)
    console.log(data);
     
    if(cityid == data.id){
        //Do nothing
    }else{
        
        //Set background
        document.body.style.backgroundImage = "url('api/../img/"+data.weather[0].main+".jpg')";

        //Remove previous data
        document.getElementById('users').innerHTML = '';
        //Create line break
        var newBreak = document.createElement("br");
        //Create main div
        var newDiv = document.createElement("div");
        newDiv.className = "weatherinfo";
        //Create Title
        var newTitle = document.createElement("h1");
        var newTitleText = document.createTextNode(data.name); 
        newTitle.appendChild(newTitleText);
        //Create icon
        var newImage = document.createElement("img");
        newImage.src = "https://openweathermap.org/img/w/"+data.weather[0].icon+".png";
        //Create Date line
        if (date.getMinutes() < 10)
        {
            var newDateText = document.createTextNode(days[date.getDay()]+' '+date.getHours()+':0'+ date.getMinutes());
        }else{
        var newDateText = document.createTextNode(days[date.getDay()]+' '+date.getHours()+':'+ date.getMinutes());
        }
        var newContentText = document.createTextNode(data.weather[0].description+' and '+String(Math.round(parseInt(data.main.temp)-kelvin), -1) + '℃'); 

        newDiv.appendChild(newTitle);
        newDiv.appendChild(newDateText);
        newDiv.appendChild(newBreak); 
        newDiv.appendChild(newImage);
        newDiv.appendChild(newContentText); 

        // add the newly created element and its content into the DOM 
        document.getElementById('users').appendChild(newDiv);

        //Call the function to get country info
        getCountryInfo(data.sys.country);

        //Set the coords and reload map
        lat = data.coord.lat;
        lon = data.coord.lon;
        initMap();
        
        //Set city id for check
        cityid = data.id;

    }
}

const succesCountryInfo = (data) => {
    console.log(data)
    //Create line break
    var newBreak = document.createElement("br");

    //Create button country info
    var newButton = document.createElement("button");
    newButton.className = "btn btn-info countryinfobutton";
    newButton.setAttribute("data-toggle", "collapse");
    newButton.setAttribute("data-target", "#countryinfo");
    newButton.innerHTML = "<strong>Country info</strong>";

    //Create subdiv
    var newSubDiv = document.createElement("div");
    newSubDiv.id = "countryinfo";
    newSubDiv.className = "collapse countryinfo";

    //Create Countrynameandflag
    var newCountryName = document.createElement("p");
    var newCountryInfoName = document.createTextNode(" " + data.name); 

    var newCountryFlagImage = document.createElement("IMG");
    newCountryFlagImage.setAttribute("src", data.flag);
    newCountryFlagImage.className = "countryFlag";

    newCountryName.appendChild(newCountryFlagImage);
    newCountryName.appendChild(newCountryInfoName);

    //Create Countrycapital
    var newCountryCapital = document.createElement("p");
    var newCountryInfoCapital = document.createTextNode(" " + data.capital); 

    var newCapitalImg = document.createElement("span");
    newCapitalImg.className = "glyphicon glyphicon-home";

    newCountryCapital.appendChild(newCapitalImg);
    newCountryCapital.appendChild(newCountryInfoCapital);

    //Create PopulationInfo
    var newCountryPopulation = document.createElement("p");
    var newCountryInfoPopulation = document.createTextNode(" " + data.population.toLocaleString()); 

    var newPopulationImg = document.createElement("span");
    newPopulationImg.className = "glyphicon glyphicon-user";

    newCountryPopulation.appendChild(newPopulationImg);
    newCountryPopulation.appendChild(newCountryInfoPopulation);

    //Create Currency
    var newCountryCurrency = document.createElement("p");
    var newCountryInfoCurrency = document.createTextNode(" " + data.currencies[0].name); 

    var newCurrencyImg = document.createElement("span");
    newCurrencyImg.className = "glyphicon glyphicon-piggy-bank";

    newCountryCurrency.appendChild(newCurrencyImg);
    newCountryCurrency.appendChild(newCountryInfoCurrency);

    // Append info
    newSubDiv.appendChild(newCountryName);
    newSubDiv.appendChild(newCountryCapital);
    newSubDiv.appendChild(newCountryPopulation);
    newSubDiv.appendChild(newCountryCurrency);

    // add the newly created element and its content into the DOM 
    document.getElementById('users').appendChild(newBreak);
    document.getElementById('users').appendChild(newButton);
    document.getElementById('users').appendChild(newSubDiv);
    document.getElementById('countryinfo').appendChild(newCountryName);
    document.getElementById('countryinfo').appendChild(newCountryCapital);
    document.getElementById('countryinfo').appendChild(newCountryPopulation);
    document.getElementById('countryinfo').appendChild(newCountryCurrency);
}

//jquery call is failed
const asycnFailCallbackHandler = (err) => {
    console.log(err);
    if(err.responseJSON.cod == 429){
            //Remove previous data
            document.getElementById('users').innerHTML = '';
            //Create line break
            var newBreak = document.createElement("br");
            //Create main div
            var newDiv = document.createElement("div");
            newDiv.className = "weatherinfo";
            //Create Title
            var newTitle = document.createElement("h1");
            var newTitleText = document.createTextNode("Exceeded request limit"); 
            newTitle.appendChild(newTitleText);
            //Create icon
            var newImage = document.createElement("img");
            newImage.src = "https://maxcdn.icons8.com/Share/icon/win10/Network//sad_cloud1600.png";
            newImage.className = "errImg";
    
            newDiv.appendChild(newTitle);
            newDiv.appendChild(newBreak); 
            newDiv.appendChild(newImage);

            // add the newly created element and its content into the DOM 
            document.getElementById('users').appendChild(newDiv);

        }else{
            //if city not found
            if(err.responseJSON.cod == 404) { }
            else{
            //Remove previous data
            document.getElementById('users').innerHTML = '';
            //Create line break
            var newBreak = document.createElement("br");
            //Create main div
            var newDiv = document.createElement("div");
            newDiv.className = "weatherinfo";
            //Create Title
            var newTitle = document.createElement("h1");
            var newTitleText = document.createTextNode("Unknown error"); 
            newTitle.appendChild(newTitleText);
            //Create icon
            var newImage = document.createElement("img");
            newImage.src = "https://maxcdn.icons8.com/Share/icon/win10/Network//sad_cloud1600.png";
            newImage.className = "errImg";

            newDiv.appendChild(newTitle);
            newDiv.appendChild(newBreak); 
            newDiv.appendChild(newImage);

            // add the newly created element and its content into the DOM 
            document.getElementById('users').appendChild(newDiv);
        }
        }
}


function getCityData(c){
    if(c.length >= 3){
        city = c;
        key = shuffle(keys);
        asyncWithJquery();
        
    }
}


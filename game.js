var images = ["img/android.png", "img/batman.jpg", "img/explorer.jpg", "img/facebook.jpg", "img/instagram.jpg", "img/soundcloud.jpg", "img/steam.jpg", "img/twitter.png", "img/xbox.png"];
var used = [];

function randomizer(size) {
    var rnd = Math.floor((Math.random() * size));
    return rnd;
}

function choicesLoop() {
    for (index = 0; index < images.length; ++index) {
        var img = document.createElement("img");
        rnd = randomizer(9);
        img.src = images[rnd];
        img.id = images[rnd];
        var imgholder = document.getElementById("img" + index);
        imgholder.appendChild(img);
        used.push(images[rnd]);
    }
}

var mainImage;
function chooseImgMain() {

    var img = document.createElement("img");
    rnd = randomizer(9);
    img.src = used[rnd];
    img.id = "picture";
    var myimg = document.getElementById("imgmain");
    myimg.appendChild(img);
    mainImage = used[rnd];
}

var score = 0;
function checkChoice(id) {
    var id = document.getElementById(id).getElementsByTagName("img")[0].id;
    if (id == mainImage) {
        score++;
        document.getElementById("score").innerHTML = score;

        $("#imgmain img").remove();
        $("#options img").remove();
        used = [];

        choicesLoop();
        chooseImgMain();
    } else {
        score--;
        document.getElementById("score").innerHTML = score;
    }
}

function average() {
    var sum = 0;
    var count = clickspd.length - 1;
    for (i = 1; i < count; i++) {
        sum += parseFloat(clickspd[i]);
    }
    var avg = sum / count;
    var avg = avg.toFixed(2);
    console.log('Avg time: ' + avg);
    document.getElementById("avg").innerHTML = "Avg: " + avg;
}

document.onkeydown = function (e) {
    e = e || window.event;
    var key = e.which || e.keyCode;
    if (key === 97) {
        var clickval = document.getElementsByTagName("div")[17].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 98) {
        var clickval = document.getElementsByTagName("div")[18].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 99) {
        var clickval = document.getElementsByTagName("div")[19].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 100) {
        var clickval = document.getElementsByTagName("div")[14].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 101) {
        var clickval = document.getElementsByTagName("div")[15].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 102) {
        var clickval = document.getElementsByTagName("div")[16].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 103) {
        var clickval = document.getElementsByTagName("div")[11].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 104) {
        var clickval = document.getElementsByTagName("div")[12].getAttribute("id");
        checkChoice(clickval);
    }
    if (key === 105) {
        var clickval = document.getElementsByTagName("div")[13].getAttribute("id");
        checkChoice(clickval);
    }
}

choicesLoop();
chooseImgMain();
